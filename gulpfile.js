var gulp = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    rename = require("gulp-rename"),
    browserSync = require('browser-sync').create(),
    sourcemaps = require('gulp-sourcemaps'),
    source = require('vinyl-source-stream'),
    browserify = require('browserify'),
    buffer = require('vinyl-buffer'),
    debowerify = require('debowerify'),
    svgmin = require('gulp-svgmin'),
    svgstore = require('gulp-svgstore'),
    inject = require('gulp-inject'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    path = require('path');



// Vendor Files


// Theme Files
var scripts = 'site/js/scripts/*.js',
    styles = 'site/sass/**/*.scss',
    html = 'site/**/*.html'
    build = ['site/css/*.css','site/js/*.js', 'site/**/*.html', 'site/img/*.*'];


/* /////////////
  Gulp Tasks
*///////////////

gulp.task('js', function () {

  var b = browserify({
    entries: './site/js/scripts/main.js',
    debug: true,
    paths: ['./bower_components/','./site/js/scripts/'],
    transform: [debowerify]
  });

  return b.bundle()
    .pipe(source('main.min.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(uglify({mangle: false})).on('error', gutil.log)
    .pipe(sourcemaps.write('./site/js/scripts/maps'))
    .pipe(gulp.dest('./site/js'));
});

gulp.task('lint', function() {
  return gulp.src(scripts)
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('sassify', function () {
  // return gulp.src(flickityCSS, { base: process.cwd() })
  // .pipe(rename({
  //   dirname: "",
  //   basename: "flickity",
  //   prefix: "_",
  //   suffix: "",
  //   extname: ".scss"
  // }))
  // .pipe(addsrc(vendorCSS))
  // .pipe(gulp.dest("sass/vendor"));
});


gulp.task('sass', function () {
  return gulp.src(styles)
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(postcss([ autoprefixer({ browsers: ['last 2 versions'] }) ]))
    .pipe(sourcemaps.write('site/css/maps'))
    .pipe(gulp.dest('site/css'))
    .pipe(browserSync.stream());
});

gulp.task('svg', function () {
  var svgs = gulp
    .src('site/img/svg/**/*.svg', { base: 'site/img/svg' })
    .pipe(svgmin(function getOptions (file) {
      var prefix = path.basename(file.relative, path.extname(file.relative));
      return {
        plugins: [{
          removeStyleElement: true,
          cleanupIDs: {
            prefix: prefix + '-',
            minify: true
          }
        }]
      }
    }))
    .pipe(rename(function (path) {
        var name = path.dirname.split(path.sep);
        name.push(path.basename);
        path.basename = name.join('-');
    }))
    .pipe(svgstore({ inlineSvg: true }));

  function fileContents (filePath, file) {
    return file.contents.toString();
  }

  return gulp
    .src('site/index.html')
    .pipe(inject(svgs, { transform: fileContents }))
    .pipe(gulp.dest('site'))
    .pipe(browserSync.stream());
});

gulp.task('js-watch', ['js'], browserSync.reload);


gulp.task('serve', function () {

  browserSync.init({
    server: {
      baseDir: 'site'
    }
  });

  gulp.watch(scripts, ['js-watch']);
  gulp.watch(styles, ['sass']);
  gulp.watch("site/img/svg/**/*.svg", ['svg']);
  gulp.watch(html).on('change', browserSync.reload);

});


gulp.task('default', ['lint', 'sass', 'svg', 'js-watch', 'serve'], function () {

});
