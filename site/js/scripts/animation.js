window.jQuery = window.$ = require('jquery');
var Velocity = require('velocity');
var ScrollMagic = require('scrollmagic');
require('../../../bower_components/velocity/velocity.ui');

module.exports = (function () {
  'use strict';

  var animate = $('#hero-animation');

  var controller = new ScrollMagic.Controller();

  var scene = new ScrollMagic.Scene({triggerElement: "#hero", triggerHook: "onLeave", duration: "200"})
  //.setVelocity("#animate", {opacity: 0}, {duration: 400})
  .on("progress", function (e) {
    var number = e.progress;
    Velocity(animate, 'stop');
    Velocity(animate, {opacity: number},{
      duration: 0,
      complete: function() {
      }
    });
	})
  .addTo(controller);

}());
