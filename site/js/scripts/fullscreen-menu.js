window.jQuery = window.$ = require('jquery');
var Velocity = require('velocity');
require('../../../bower_components/velocity/velocity.ui');

module.exports = (function () {
   'use strict';

  function showContent(button) {

    var contentTarget = button.attr('data-content');
    var content = $('#'+contentTarget);
    var other = content.siblings('.cv-content');

    if (!button.attr('data-toggled') || button.attr('data-toggled') == 'false'){

      button.attr('data-toggled','true');
      button.siblings().attr('data-toggled','false');

      Velocity(content, 'stop');
      Velocity(content, 'transition.slideUpIn', {
        easing: [100,25],
        duration: 600,
        begin: function() {
          Velocity(other, {opacity: 0}, {duration: 0, display: 'none'});
        }
      });
    }
    else if (button.attr('data-toggled') == 'true'){

      //button.attr('data-toggled','false');

    }

    return;
  }

  $('#cv nav button').on('click', function(){
    showContent($(this));
  });

}());
