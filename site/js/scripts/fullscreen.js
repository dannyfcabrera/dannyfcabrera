window.jQuery = window.$ = require('jquery');
var Velocity = require('velocity');

module.exports = (function () {
   'use strict';

  function addButton(button,view) {
   var buttonTemp = '<button class="close icon transparent" type="button"><span class="sr-only">Close</span><svg class="icon"><use xlink:href="#button-close" /></svg></button>';
   $( buttonTemp ).appendTo( view );
   view.on('click', ".close", function(){

     if (button.attr('data-toggled') == 'true'){

       Velocity(view, 'stop');
       Velocity(view, { top: '100%' }, {
         easing: [100,25],
         duration: 600,
         begin: function() {
          view.children('.close').remove();
          button.attr('data-toggled','false');
         },
         complete: function() {
          // close complete
          $('body').removeClass('no-scroll');
         }
       });
     }

   });
   return;
  }


  function toggleFullscreen(button) {

    var viewTarget = button.attr('data-trigger');
    var view = $('.fullscreen').attr("id", viewTarget);

    if (!button.attr('data-toggled') || button.attr('data-toggled') == 'false'){

      Velocity(view, 'stop');
      Velocity(view, { top: 0 }, {
        easing: [100,25],
        duration: 600,
        begin: function() {
          $('body').addClass('no-scroll');
          button.attr('data-toggled','true');
          addButton(button,view);
          Velocity($('.fullscreen .close'), {rotateZ: '-45deg'}, {duration: 0});
          Velocity($('.fullscreen .close'), {opacity: 1, rotateZ: '0deg'}, {duration: 600});
        },
        complete: function() {
          // complete
        }
      });
    }
    else if (button.attr('data-toggled') == 'true'){

      // button.attr('data-toggled','false');

    }

    return;
  }

  $('button.trigger').on('click', function(){
    toggleFullscreen($(this));
  });

}());
